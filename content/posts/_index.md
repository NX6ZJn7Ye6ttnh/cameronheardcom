---
title: "Archive"
date: 2023-02-19
url: "/archive"
---

This page automatically lists all posts in `/content/posts`. It is generated by `/themes/hugo-zones/layouts/_default/list.html`.

This markdown file is `/content/posts/_index.md`.

The url for this page is customized by setting the `url` attribute in the markdown file. The default url would be `posts`, as that is the name of the parent directory, but I changed it to `archive` to match with Zoner's naming.