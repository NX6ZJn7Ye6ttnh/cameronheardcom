---
title: Third post
date: 2023-02-19
draft: false
---

This is an example of a post in a flat branch bundle. The markdown file for this page is `/content/posts/post3.md`

Here is an image that lives in `/static/img/post3/`