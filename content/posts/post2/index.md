---
title: Second post
date: 2023-02-18
draft: false
---

The second post! This post is its own leaf bundle. All the resources for this post are included in the post2 folder.

This image is in the `/content/post2` folder, so we can reference it by relative file path: