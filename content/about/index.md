---
title: "About"
draft: false
---
```
+--------------------------------------+
|Cameron Heard                         |
+--------------------------------------+
|Network Engineer                      |
+--------------------------------------+
|Linux stuff                           |
+--------------------------------------+
|CyberSec                              |
+--------------------------------------+
|Cool Stuff                            |
+--------------------------------------+
```
# TO-DO
- Create n8n automation to rebuild when a new file is detected in the content directory
- Create Olivetin button text input to run `hugo new post '$Input'.md`
